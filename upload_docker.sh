#!/usr/bin/env bash
# This file tags and uploads an image to Docker Hub

# Assumes that an image is built via `run_docker.sh`

# Step 1:
# Create dockerpath
dockerpath="udacityproject"
username="ardayavas"
version="version1.0"

# Step 2:  
# Authenticate & tag
echo "Docker ID and Image: $dockerpath"
docker tag ${dockerpath} ${username}/${dockerpath}:${version}

docker login --username ${username}

# Step 3:
# Push image to a docker repository
docker push ${username}/${dockerpath}:${version}