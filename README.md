[![CircleCI](https://circleci.com/gh/ardayavas/ml_project.svg?style=svg)](https://circleci.com/gh/ardayavas/ml_project)

# Operationalize a Machine Learning Microservice API

This project deploys pre-trained service on Kubernetes that predicts housing prices in Boston.

## Installation

### 1. Clone Project

* ```git clone https://github.com/ardayavas/ml_project.git```
* ```cd project-ml-microservice-kubernetes```

### 1. Create and Activate Virtual Environment

* ```make setup```
* Activate virtual environment using```source ~/.devops/bin/activate```
* ```make install```

### 2. Configure Variables

* Configure username variable in ```./upload_docker.sh```.
* Configure username variable in ```./run_kubernetes.sh```.

### 3. Run files
* ```./run_docker.sh```
* ```./upload_docker.sh```
* ```./run_kubernetes.sh```


## Usage
Run ```make_prediction.sh``` to make predictions.

## Project Files

* Makefile is used to prepare local Python environment and linting.
* .circleci/config.yml is used for linting.
* app.py is the service application, running on flask.
* model_data/* files are necessary for pre-trained prediction algorithm.
* requirements.txt is required for downloading necessary Python packages.
* Dockerfile contains necessary steps to build docker image.
* run_docker.sh creates docker image locally.
* upload_docker.sh tags the docker image uploads to the docker hub.
* run_kubernetes.sh downloads the tagged image from docker hub and runs locally on a forwarded port.


