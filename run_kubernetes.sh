#!/usr/bin/env bash

# This tags and uploads an image to Docker Hub

# Step 1:
# This is your Docker ID/path
username="ardayavas"
path="udacityproject"
version="version1.0"

dockerpath="${username}/${path}:${version}"

# Step 2
# Run the Docker Hub container with kubernetes
kubectl run ${path} --image=${dockerpath} --port=80 --generator=run-pod/v1

# Step 3:
# List kubernetes pods
kubectl get pods

while [ `kubectl get pods | grep ${path} | awk '{print $3}'` == "ContainerCreating" ]
do
    echo "Lets wait until pod is ready"
    sleep 1
done


# Step 4:
# Forward the container port to a host
kubectl port-forward ${path} 8000:80 &
sleep 1
`pwd`/make_prediction.sh